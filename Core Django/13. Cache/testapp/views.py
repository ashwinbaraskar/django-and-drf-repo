from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import TaskCreate
from .models import Task
from django.core.paginator import Paginator


# Create your views here.


def home(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            fm = TaskCreate(request.POST)

            if fm.is_valid():
                # task = fm.cleaned_data['task']
                # st = fm.cleaned_data['status']
                # reg = Task(task=task, status=st)
                # reg.save()
                fm.save()
                fm = TaskCreate()
        else:
            fm = TaskCreate()
        return render(request, 'home.html', {'form': fm})
    else:
        return redirect('/login/')


from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


def show_data(request):
    data = Task.objects.all()
    task_paginator = Paginator(data, 5)
    page_num = request.GET.get('page')
    number = list(range(1, 12))
    page = task_paginator.get_page(page_num)
    return render(request, 'showdata.html', {'data': page, 'count': task_paginator.count, 'number':number})


def delete_data(request, pk):
    # if request.method == 'POST':
    t = Task.objects.get(id=pk)
    t.delete()
    return redirect('/')


def update_data(request, pk):
    if request.method == 'POST':
        pi = Task.objects.get(id=pk)
        fm = TaskCreate(request.POST, instance=pi)
        if fm.is_valid():
            fm.save()
    else:
        pi = Task.objects.get(id=pk)
        fm = TaskCreate(instance=pi)

    return render(request, 'updatetask.html', {'form': fm})


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']
        labels = {'email': 'Email'}


def register(request):
    if request.method == "POST":
        fm = RegisterForm(request.POST)
        if fm.is_valid():
            messages.success(request, "Account Created Successfully !!!")
            fm.save()
    else:
        fm = RegisterForm()
    return render(request, 'register.html', {'form': fm})


from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout


def user_login(request):
    if not request.user.is_authenticated:
        if request.method == 'POST':
            fm = AuthenticationForm(request=request, data=request.POST)
            if fm.is_valid():
                uname = fm.cleaned_data['username']
                upass = fm.cleaned_data['password']
                user = authenticate(username=uname, password=upass)
                if user is not None:
                    login(request, user)
                    messages.success(request, 'Logged in successfully !!')
                    return redirect('/show')
        else:
            fm = AuthenticationForm()
        return render(request, 'login.html', {'form': fm})
    else:
        return redirect('/show')


def user_logout(request):
    logout(request)
    return redirect('/')
