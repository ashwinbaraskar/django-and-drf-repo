from django import forms
from .models import Task


class TaskCreate(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['task', 'status']
        # widgets = {
        #     'task': forms.Textarea(attrs={'class':'form-control'}),
        #     'status': forms.CharField(attrs={'class':'form-control'})
        #
        # }