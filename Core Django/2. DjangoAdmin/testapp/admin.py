from django.contrib import admin
from testapp.models import Customer, Order


# Register your models here.
class CustomerAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'phone', 'salary', 'date_created']


class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'status', 'customer', 'date_created']


admin.site.register(Customer, CustomerAdmin)
admin.site.register(Order, OrderAdmin)
