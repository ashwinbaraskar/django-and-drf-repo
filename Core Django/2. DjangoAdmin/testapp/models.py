from django.db import models


# Create your models here.
class Customer(models.Model):
    name = models.CharField(max_length=100, null=True)
    phone = models.CharField(max_length=100, null=True)
    salary = models.CharField(max_length=100, null=True)
    date_created = models.DateField(auto_now_add=True, null=True)

    def __str__(self):
        return self.name


class Order(models.Model):
    STATUS = (
        ('Pending', 'Pending'),
        ('Out for delivery', 'Out for delivery'),
        ('Delivered', 'Delivered'),
    )

    status = models.CharField(max_length=100, null=True, choices=STATUS)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='orderref')
    date_created = models.DateField(auto_now_add=True, null=True)

    def __str__(self):
        return self.status