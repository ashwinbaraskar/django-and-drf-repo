from django.shortcuts import render, redirect
from .forms import TaskCreate
from .models import Task


def home(request):
    if request.method == 'POST':
        fm = TaskCreate(request.POST)

        if fm.is_valid():
            # task = fm.cleaned_data['task']
            # st = fm.cleaned_data['status']
            # reg = Task(task=task, status=st)
            # reg.save()
            fm.save()
            fm = TaskCreate()
    else:
        fm = TaskCreate()
    return render(request, 'home.html', {'form': fm})


def show_data(request):
    data = Task.objects.all()
    return render(request, 'showdata.html', {'data':data})


def delete_data(request, pk):
    # if request.method == 'POST':
    t = Task.objects.get(id=pk)
    t.delete()
    return redirect('/')


def update_data(request, pk):
    if request.method == 'POST':
        pi = Task.objects.get(id=pk)
        fm = TaskCreate(request.POST, instance=pi)
        if fm.is_valid():
            fm.save()
    else:
        pi = Task.objects.get(id=pk)
        fm = TaskCreate(instance=pi)

    return render(request, 'updatetask.html', {'form': fm })






