from django.db import models


# Create your models here.

class Task(models.Model):
    STATUS = (
        ('Pending', 'Pending'),
        ('Done', 'Done'),
        ('In-Progress', 'In-Progress'),
    )
    task = models.TextField()
    status = models.CharField(max_length=100, null=True, choices=STATUS)
