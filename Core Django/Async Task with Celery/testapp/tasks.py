from __future__ import absolute_import, unicode_literals

import time

from celery import shared_task


@shared_task
def summation(a, b):
    time.sleep(10)
    return a + b
