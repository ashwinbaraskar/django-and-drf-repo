from django.shortcuts import render
from .tasks import summation
# Create your views here.


def about_view(request):
    # t = summation(5, 6)
    t = summation.delay(5, 6)

    return render(request, 'Home.html', {'task': t})
