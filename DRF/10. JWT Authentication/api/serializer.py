from rest_framework import serializers
from .models import Category, Product
from drf_writable_nested import WritableNestedModelSerializer


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['title']


class ProductSerializer(WritableNestedModelSerializer,serializers.ModelSerializer):
    # category = serializers.StringRelatedField()
    category = CategorySerializer()


    class Meta:
        model = Product
        fields = ['title', 'category', 'created_at', 'updated_at']


