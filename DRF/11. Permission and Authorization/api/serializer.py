from rest_framework import serializers
from .models import Category, Product
from drf_writable_nested import WritableNestedModelSerializer


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'title']


class ProductSerializer(WritableNestedModelSerializer,serializers.ModelSerializer):
    # category = serializers.StringRelatedField()
    category = CategorySerializer()


    class Meta:
        model = Product
        fields = ['id', 'title', 'category', 'created_at', 'updated_at']


