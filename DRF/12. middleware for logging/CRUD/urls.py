from django.contrib import admin
from django.urls import path, include
from api import views

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework.routers import DefaultRouter

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from rest_framework_simplejwt.views import TokenVerifyView

schema_view = get_schema_view(
   openapi.Info(
      title="API Documentation",
      default_version='v1',

   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)

router = DefaultRouter()

router.register('product', views.ProductModelViewSet, basename='product')
router.register('category', views.CategotyModelViewSet, basename='category')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),

    # path('swagger(?P<format>\.json|\.yaml)', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),

    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/verify/', TokenVerifyView.as_view(), name='token_verify'),

]
