from .models import Category, Product
from .serializer import CategorySerializer, ProductSerializer
from rest_framework import viewsets

from rest_framework.pagination import PageNumberPagination
from rest_framework.generics import ListAPIView
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.permissions import IsAuthenticated, IsAdminUser, DjangoModelPermissions
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.authentication import BasicAuthentication


class MyPageNumberPagination(PageNumberPagination):
    page_size = 5


class ProductModelViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    pagination_class = MyPageNumberPagination
    filter_backends = [filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter]
    # filter_backends = [DjangoFilterBackend]
    filterset_fields = ['title', 'category', 'created_at', 'updated_at']
    search_fields = ['title']
    ordering_fields = ['created_at', 'title']
    # authentication_classes = [JWTAuthentication]
    # permission_classes = [IsAuthenticated]
    authentication_classes = [BasicAuthentication]
    # permission_classes = [IsAdminUser]
    permission_classes = [DjangoModelPermissions]

class CategotyModelViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    pagination_class = MyPageNumberPagination
    filter_backends = [filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter]
    # filter_backends = [DjangoFilterBackend]
    search_fields = ['title', 'created_at', 'updated_at']
    filterset_fields = ['title', 'created_at', 'updated_at']
    ordering_fields = ['created_at']
    # authentication_classes = [JWTAuthentication]
    # permission_classes = [IsAuthenticated]
    # permission_classes = [IsAdminUser]
    authentication_classes = [BasicAuthentication]
    permission_classes = [DjangoModelPermissions]