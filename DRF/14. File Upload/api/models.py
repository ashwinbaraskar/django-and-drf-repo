from django.db import models


class Category(models.Model):
    CAT = (
        ('Electronics', 'Electronics'),
        ('Fashion and Beauty', 'Fashion and Beauty'),
        ('Groceries', 'Groceries'),
        ('Books and Education', 'Books and Education'),
        ('Automotive', 'Automotive'),
        ('Pharmacy', 'Pharmacy')
    )

    title = models.CharField(max_length=100, null=True, choices=CAT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=100, null=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='product')
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    price = models.IntegerField()
    updated_at = models.DateTimeField(auto_now=True)
    image = models.ImageField(upload_to='photos', max_length=254)

    def __str__(self):
        return self.title
