from django.contrib import admin
from .models import Quotes


class StudentAdmin(admin.ModelAdmin):
    list_display = ['id', 'created_by', 'city', 'publish', 'created_at']


admin.site.register(Quotes, StudentAdmin)
