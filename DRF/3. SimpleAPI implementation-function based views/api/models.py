from django.db import models


class Quotes(models.Model):
    created_by = models.CharField(max_length=100, null=True)
    city = models.CharField(max_length=100, null=True)
    publish = models.DateTimeField(null=True)
    created_at = models.DateTimeField(auto_now=True, null=True)
