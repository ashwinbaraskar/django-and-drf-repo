from rest_framework import serializers
from .models import Quotes


class QuotesSerializer(serializers.Serializer):
    created_by = serializers.CharField(max_length=100)
    city = serializers.CharField(max_length=100)
    publish = serializers.DateTimeField()
    created_at = serializers.DateTimeField()




