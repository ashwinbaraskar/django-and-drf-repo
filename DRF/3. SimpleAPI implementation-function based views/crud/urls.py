from django.contrib import admin
from django.urls import path
from api import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('quotes', views.quotes_list),
    path('quotes/<int:pk>', views.quotes_detail),

]
