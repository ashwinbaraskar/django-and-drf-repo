from .models import Category, Product
from .serializer import CategorySerializer, ProductSerializer
from rest_framework import viewsets

from rest_framework.pagination import PageNumberPagination
from rest_framework.generics import ListAPIView


class MyPageNumberPagination(PageNumberPagination):
    page_size = 2

class ProductModelViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    pagination_class = MyPageNumberPagination


class CategotyModelViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    pagination_class = MyPageNumberPagination
