from django.contrib import admin
from .models import Category, Product


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['title']


class ProductAdmin(admin.ModelAdmin):
    # list_display = ['id', 'name', 'created_at', 'created_by', 'category']
    list_display = ['title', 'category', 'created_at', 'updated_at']


admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
