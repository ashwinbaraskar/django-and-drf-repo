from rest_framework import serializers
from .models import Category, Product


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    # cat = CategorySerializer(many=True, read_only=True)
    category = serializers.StringRelatedField()
    depth = 2

    class Meta:
        model = Product
        fields = ['title', 'category', 'created_at', 'updated_at']
