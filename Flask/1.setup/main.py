from flask import Flask, render_template, redirect, url_for, request

app = Flask(__name__)


# @app.route('/')
# def hello_world():
#     return 'Hello, World!'


@app.route('/hello')
def hello_world1():
    return '<h1>Hello, World!!!!</h1>'


# app.add_url_rule('/', 'hello', hello_world1)


int = 10
list = [10, 20, 30]


@app.route('/about/')
def about():
    return render_template('about.html', v1=int, v2=list)


@app.route('/blog/<int:postID>')
def show_blog(postID):
    return 'Blog Number is %d' % postID


@app.route('/rev/<float:revNo>')
def revision(revNo):
    return 'Revision Number %f' % revNo


#########################################################
@app.route('/admin')
def hello_admin():
    return 'Hello Admin'


@app.route('/guest/<guest>')
def hello_guest(guest):
    return 'Hello %s as Guest' % guest


@app.route('/user/<name>')
def hello_user(name):
    if name == 'admin':
        return redirect(url_for('hello_admin'))
    else:
        return redirect(url_for('hello_guest', guest=name))


###################################################################


@app.route('/success/<name>')
def success(name):
    return 'welcome %s' % name


@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        user = request.form['nm']
        # user = request.args.get('nm')
        return redirect(url_for('success', name=user))
    return render_template('login.html')

@app.route('/result')
def result():
   dict = {'phy':50,'che':60,'maths':70}
   return render_template('result.html', result = dict)


if __name__ == '__main__':
    app.run(debug=True)
