from django.contrib import admin
from django.urls import path, include
from api import views

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework.routers import DefaultRouter

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from rest_framework_simplejwt.views import TokenVerifyView

schema_view = get_schema_view(
   openapi.Info(
      title="API Documentation",
      default_version='v1',

   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)

router = DefaultRouter()

router.register('candidate', views.CandidateModelViewSet, basename='candidate')
router.register('recruiter', views.RecruiterModelViewSet, basename='recruiter')
router.register('applyjob', views.ApplyJobModelViewSet, basename='applyjob')
router.register('applyjob1', views.ApplyJob1ModelViewSet, basename='applyjob1')

router.register(r"user", views.UserViewSet, basename="user")


from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('get_data/', views.get_job_data, name='get_date'),
    # path('profile/', views.profile, name='profile'),
    path('test/', views.test_api_view, name='test'),

    path('list_create/', views.ListCreateAPI.as_view(), name='list_create'),

    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    path('api/password_reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),

    path('api/change-password/', views.ChangePasswordView.as_view(), name='change-password'),
    path('api/create/', views.SnippetList.as_view(), name='create'),

    path('api-auth/', include('rest_framework.urls')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    path('__debug__/', include('debug_toolbar.urls')),


]

