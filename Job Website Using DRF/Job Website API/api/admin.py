from django.contrib import admin
from .models import Candidate, Recruiter, Apply_Job


class CandidateAdmin(admin.ModelAdmin):
    list_display = ('name', 'gender', 'date_of_birth', 'email')


class RecruiterAdmin(admin.ModelAdmin):
    # list_display = ['id', 'name', 'created_at', 'created_by', 'category']
    list_display = ('title', 'description')


class ApplyJobAdmin(admin.ModelAdmin):
    # list_display = ['id', 'name', 'created_at', 'created_by', 'category']
    list_display = ('location', 'candidate', 'job')


admin.site.register(Candidate, CandidateAdmin)
admin.site.register(Recruiter, RecruiterAdmin)
admin.site.register(Apply_Job, ApplyJobAdmin)
