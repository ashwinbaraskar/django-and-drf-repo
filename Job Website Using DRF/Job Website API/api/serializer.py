from rest_framework import serializers
from .models import Candidate, Recruiter, Apply_Job
from drf_writable_nested import WritableNestedModelSerializer

from rest_framework import serializers
from django.contrib.auth import get_user_model


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = "__all__"


class CandidateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Candidate
        fields = '__all__'
        # fields = ['id', 'name']



class RecruiterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recruiter
        fields = '__all__'


class ApplyJobSerializer(WritableNestedModelSerializer, serializers.ModelSerializer):
    # candidate = serializers.StringRelatedField()
    # job = serializers.StringRelatedField()
    candidate = CandidateSerializer()
    job = RecruiterSerializer()

    class Meta:
        model = Apply_Job
        fields = ['location', 'candidate', 'job', 'created_at']


class ApplyJob1Serializer(WritableNestedModelSerializer,serializers.ModelSerializer):
    candidate = CandidateSerializer()
    job = RecruiterSerializer()

    class Meta:

        model = Apply_Job
        fields = ['candidate', 'job']

from rest_framework import serializers
from django.contrib.auth.models import User

class ChangePasswordSerializer(serializers.Serializer):
    model = User

    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)