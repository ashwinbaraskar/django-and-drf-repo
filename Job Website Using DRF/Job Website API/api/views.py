import traceback



from .models import Candidate, Recruiter, Apply_Job
from .serializer import CandidateSerializer, RecruiterSerializer, ApplyJobSerializer, ApplyJob1Serializer
from rest_framework import viewsets

from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser, DjangoModelPermissions
from rest_framework.pagination import PageNumberPagination


class MyPageNumberPagination(PageNumberPagination):
    page_size = 5



from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class SnippetList(APIView):
    """
    List all snippets, or create a new snippet.
    """
    def post(self, request, format=None):
        serializer = CandidateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CandidateModelViewSet(viewsets.ModelViewSet):
    queryset = Candidate.objects.all()
    serializer_class = CandidateSerializer
    pagination_class = MyPageNumberPagination
    authentication_classes = [BasicAuthentication]
    permission_classes = [DjangoModelPermissions]


class RecruiterModelViewSet(viewsets.ModelViewSet):
    queryset = Recruiter.objects.all()
    serializer_class = RecruiterSerializer
    pagination_class = MyPageNumberPagination
    authentication_classes = [BasicAuthentication]
    permission_classes = [DjangoModelPermissions]


class ApplyJobModelViewSet(viewsets.ModelViewSet):
    queryset = Apply_Job.objects.all()
    serializer_class = ApplyJobSerializer
    pagination_class = MyPageNumberPagination
    # authentication_classes = [BasicAuthentication]
    # permission_classes = [DjangoModelPermissions]

class ApplyJob1ModelViewSet(viewsets.ModelViewSet):
    queryset = Apply_Job.objects.all()
    serializer_class = ApplyJob1Serializer
    pagination_class = MyPageNumberPagination


from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import get_user_model
from api.serializer import UserSerializer
class UserViewSet(viewsets.ModelViewSet):
    """
    UserModel View.
    """

    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()

from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response
from django.contrib.auth.models import User
from api.serializer import ChangePasswordSerializer
from rest_framework.permissions import AllowAny

class ChangePasswordView(generics.UpdateAPIView):
    """
    An endpoint for changing password.
    """
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (AllowAny,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }

            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


from django.views.decorators.csrf import csrf_exempt

from django.http import JsonResponse

@csrf_exempt
def get_job_data(request):

    try:
        if request.method in ["GET", "POST"]:
            d = {}
            apply = Apply_Job.objects.select_related('candidate','job')
            for i in apply:
                d['candidate_id'] = i.candidate.id
                d['candidate_name'] = i.candidate.name
                d['job_id'] = i.job.id
                d['job_title'] = i.job.title
                d['job_desc'] = i.job.description
                d['applied_date'] = i.created_at

          

                return JsonResponse({
                    "response": d
                })
        else:
            return JsonResponse({
                "message": 'Method not allowed',
                "status": 405
            },status=405)


    except Exception as e:
        print(e, traceback.format_exc())
        return JsonResponse({
            "error": str(traceback.format_exc()),
            "message": "Something went wrong",
            "status": 403
        })


# def profile(request):
#     data = {
#         'name': 'Raghav',
#         'location': 'India',
#         'is_active': False,
#         'count': 28
#     }
#     return JsonResponse(data)


class ApiView():
    def get(self):
        print("get")
    def post(self):
        print("post")



@csrf_exempt
def test_api_view(request):
    obj = ApiView()
    if request.method =="GET":
        obj.get()
        return JsonResponse({
            "response": 'get method is called'
        })

    elif request.method == "POST":
        obj.post()
        return JsonResponse({
            "response": "Post method is called"
        })


class ListCreateAPI(generics.ListCreateAPIView):
    queryset = Apply_Job.objects.select_related('candidate','job')

    def list(self, request):
        # Note the use of `get_queryset()` instead of `self.queryset`
        d = {}
        queryset = self.get_queryset()
        for i in queryset:
            d['candidate_id'] = i.candidate.id
            d['candidate_name'] = i.candidate.name
            d['job_id'] = i.job.id
            d['job_title'] = i.job.title
            d['job_desc'] = i.job.description
            d['applied_date'] = i.created_at
        return Response(d)

