# Django and DRF Repo

Admin Panel and other login
Username:admin
Password:admin

## Core Django concepts	
		
1	Django MVT Architecture

3	Django Admin

4	Database

6	ORM

5	Class based views vs Functional Views

7	Paginations

8	Authentication (Session)

9	Forms

10	Django Debug Toolbar

11	Template tags

12	Model relations

13	Cache

14	Middleware

15	Authorization



## Django Rest Framework			

1	REST API Basics

2	Consuming REST API

3	Simple API implementation with function based views

4	Simple API implementation with class based views

5	API Documentation Swagger

6	Generic Views

7	Nested Serializer

8	Pagination

9	Filters and search

10	JWT Authentication

11	Permissions/Authorization

12	Logging

13	Caching

14	File Upload
	
	

